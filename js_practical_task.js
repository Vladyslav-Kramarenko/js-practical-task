'use strict';

/**
 * You must return a date that comes in a predetermined number of seconds after 01.06.2020 00:00:002020
 * @param {number} seconds
 * @returns {Date}
 *
 * @example
 *      31536000 -> 01.06.2021
 *      0 -> 01.06.2020
 *      86400 -> 02.06.2020
 */
function secondsToDate(seconds) {
    const startDate = new Date('2020-06-01T00:00:00');
    const targetDate = new Date(startDate.getTime() + seconds * 1000);
    console.log(targetDate)
    return targetDate;
}

/**
 * You must create a function that returns a base 2 (binary) representation of a base 10 (decimal) string number
 * ! Numbers will always be below 1024 (not including 1024)
 * ! You are not able to use parseInt
 * @param {number} decimal
 * @return {string}
 *
 * @example
 *      5 -> "101"
 *      10 -> "1010"
 */
function toBase2Converter(decimal) {
    if (decimal === 0) {
        return '0';
    }

    let binary = '';
    let num = decimal;

    while (num > 0) {
        binary = (num % 2) + binary;
        num = Math.floor(num / 2);
    }

    return binary;
}

/**
 * You must create a function that takes two strings as arguments and returns the number of times the first string
 * is found in the text.
 * @param {string} substring
 * @param {string} text
 * @return {number}
 *
 * @example
 *      'a', 'test it' -> 0
 *      't', 'test it' -> 2
 *      'T', 'test it' -> 2
 */
function substringOccurrencesCounter(substring, text) {
    // g - global
    // i - case-insensitive
    const regex = new RegExp(substring, 'gi');
    const matches = text.match(regex);
    console.log(matches);
    return matches ? matches.length : 0;
}

/**
 * You must create a function that takes a string and returns a string in which each character is repeated once.
 *
 * @param {string} string
 * @return {string}
 *
 * @example
 *      "Hello" -> "HHeelloo"
 *      "Hello world" -> "HHeello  wworrldd" // o, l is repeated more then once. Space was also repeated
 */
//TODO always should be 2 letters not more or less
function repeatingLetters(string) {
    let result = '';
    let lastChar = '';
    for (let i = 0; i < string.length; i++) {
        if (string[i] !== lastChar) {
            result += string[i] + string[i];
        }
        lastChar = string[i];
    }
    return result;
}

/**
 * You must write a function redundant that takes in a string str and returns a function that returns str.
 * ! Your function should return a function, not a string.
 *
 * @param {string} str
 * @return {function}
 *
 * @example
 *      const f1 = redundant("apple")
 *      f1() ➞ "apple"
 *
 *      const f2 = redundant("pear")
 *      f2() ➞ "pear"
 *
 *      const f3 = redundant("")
 *      f3() ➞ ""
 */
function redundant(str) {
    return function () {
        return str;
    };
}

/**
 * https://en.wikipedia.org/wiki/Tower_of_Hanoi
 *
 * @param {number} disks
 * @return {number}
 */
function towerHanoi(disks) {
    return Math.pow(2, disks) - 1;
}

/**
 * You must create a function that multiplies two matricies (n x n each).
 *
 * @param {array} matrix1
 * @param {array} matrix2
 * @return {array}
 *
 */
function matrixMultiplication(matrix1, matrix2) {
    const result = [];
    for (let i = 0; i < matrix1.length; i++) {
        result[i] = [];
        for (let j = 0; j < matrix2[0].length; j++) {
            let sum = 0;
            for (let k = 0; k < matrix1[0].length; k++) {
                // Multiply the corresponding elements from matrix1 row and matrix2 column,
                // and accumulate the sum of their products
                sum += matrix1[i][k] * matrix2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
}

/**
 * Create a gather function that accepts a string argument and returns another function.
 * The function calls should support continued chaining until order is called.
 * order should accept a number as an argument and return another function.
 * The function calls should support continued chaining until get is called.
 * get should return all of the arguments provided to the gather functions as a string in the order specified in the order functions.
 *
 * @param {string} str
 * @return {string}
 *
 * @example
 *      gather("a")("b")("c").order(0)(1)(2).get() ➞ "abc"
 *      gather("a")("b")("c").order(2)(1)(0).get() ➞ "cba"
 *      gather("e")("l")("o")("l")("!")("h").order(5)(0)(1)(3)(2)(4).get()  ➞ "hello"
 */
function gather(str) {
    let accumulatedString = str || ""; // Initialize the accumulated string with the provided string or an empty string

    // Function to add the next string to the accumulated string
    function gatherFn(nextStr) {
        accumulatedString += nextStr || "";
        return gatherFn;
    }

    // Function to specify the order of characters
    function order(arg) {

        const orderedArguments = [];
        orderedArguments.push(arg);

        // Function to add the next arg to the characters order array
        const orderFn = function (nextArg) {
            orderedArguments.push(nextArg);
            return orderFn;
        };

        // Function to retrieve the ordered string
        orderFn.get = function () {
            let result = "";
            if (orderedArguments.length === 1 && orderedArguments[0]===undefined) return accumulatedString;
            for (let i = 0; i < orderedArguments.length; i++) {
                const index = orderedArguments[i];
                result += accumulatedString[index] || ""; // Retrieve characters from the accumulated string based on the specified indexes

            }
            return result;
        };

        return orderFn;
    }

    gatherFn.order = order; // Attach the order function as a property to the gatherFn function

    return gatherFn; // Return the gatherFn function as the final result
}

console.log(gather("a")("b")("c").order(0)(1)(2).get()); // Output: "abc"

module.exports = {
    secondsToDate,
    toBase2Converter,
    substringOccurrencesCounter,
    repeatingLetters,
    redundant,
    towerHanoi,
    matrixMultiplication,
    gather
};