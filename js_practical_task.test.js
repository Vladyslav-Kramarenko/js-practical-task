const {
    secondsToDate,
    toBase2Converter,
    substringOccurrencesCounter,
    repeatingLetters,
    redundant,
    towerHanoi,
    matrixMultiplication,
    gather
} = require('./js_practical_task');

/* secondsToDate tests*/
test('secondsToDate - 31536000 seconds after 01.06.2020 00:00:00', () => {
    const date = secondsToDate(31536000);
    //31536000 s = 365 D
    const expectedDate = new Date("2021-06-01T00:00:00");
    expect(date.getTime()).toBe(expectedDate.getTime());
});

test('secondsToDate - 0 seconds after 01.06.2020 00:00:00', () => {
    const date = secondsToDate(0);
    const expectedDate = new Date('2020-06-01T00:00:00');
    expect(date.getTime()).toBe(expectedDate.getTime());
});

test('secondsToDate - 86400 seconds after 01.06.2020 00:00:00', () => {
    //86400s  = 24 H
    const date = secondsToDate(86400);
    const expectedDate = new Date('2020-06-02T00:00:00');
    expect(date.getTime()).toBe(expectedDate.getTime());
});

/* toBase2Converter tests*/

test('toBase2Converter - decimal number 5 should be converted to binary "101"', () => {
    const binary = toBase2Converter(5);
    expect(binary).toBe('101');
});

test('toBase2Converter - decimal number 10 should be converted to binary "1010"', () => {
    const binary = toBase2Converter(10);
    expect(binary).toBe('1010');
});

test('toBase2Converter - decimal number 0 should be converted to binary "0"', () => {
    const binary = toBase2Converter(0);
    expect(binary).toBe('0');
});

/* substringOccurrencesCounter tests*/

test('substringOccurrencesCounter - should return 0 when substring is not found in the text', () => {
    const count = substringOccurrencesCounter('a', 'test it');
    expect(count).toBe(0);
});

test('substringOccurrencesCounter - should count the number of occurrences of substring in the text (case-insensitive)', () => {
    const count = substringOccurrencesCounter('t', 'test it');
    expect(count).toBe(3);
});

test('substringOccurrencesCounter - should count the number of occurrences of substring in the text (case-insensitive)', () => {
    const count = substringOccurrencesCounter('T', 'test it');
    expect(count).toBe(3);
});

test('substringOccurrencesCounter - should count the number of occurrences of substring in the text (case-insensitive)', () => {
    const count = substringOccurrencesCounter('te', 'test it');
    expect(count).toBe(1);
});

/* redundant tests*/

test('redundant - should return a function that returns the original string', () => {
    const f1 = redundant("apple");
    expect(f1()).toBe("apple");

    const f2 = redundant("pear");
    expect(f2()).toBe("pear");

    const f3 = redundant("");
    expect(f3()).toBe("");
});

/* repeatingLetters tests*/

test('repeatingLetters - should repeat each character in the string once', () => {
    const result1 = repeatingLetters("Hello");
    expect(result1).toBe("HHeelloo");

    const result2 = repeatingLetters("Hello world");
    expect(result2).toBe("HHeelloo  wwoorrlldd");
});

test('repeatingLetters - should handle empty string', () => {
    const result = repeatingLetters("");
    expect(result).toBe("");
});

test('repeatingLetters - should handle string with only one character', () => {
    const result = repeatingLetters("A");
    expect(result).toBe("AA");
});

test('repeatingLetters - should handle string with more than 2 repeating character', () => {
    const result = repeatingLetters("AAAAA");
    expect(result).toBe("AA");
});

/* towerHanoi tests*/
test(
    'towerHanoi - should return minimum numbers of moves to move the entire stack to the last rod', () => {
        const result = towerHanoi(3);
        expect(result).toBe(7);
    }
);

/* matrixMultiplication tests*/

test('matrixMultiplication - should multiply two matrices correctly', () => {
    const matrix1 = [[1, 2], [3, 4]];
    const matrix2 = [[5, 6], [7, 8]];

    const result = matrixMultiplication(matrix1, matrix2);

    expect(result).toEqual([[19, 22], [43, 50]]);
});

test('matrixMultiplication - should handle empty matrices', () => {
    const matrix1 = [];
    const matrix2 = [];

    const result = matrixMultiplication(matrix1, matrix2);

    expect(result).toEqual([]);
});

test('matrixMultiplication - should handle matrices of different sizes', () => {
    const matrix1 = [[1, 2], [3, 4]];
    const matrix2 = [[5, 6, 7], [8, 9, 10]];

    const result = matrixMultiplication(matrix1, matrix2);

    expect(result).toEqual([[21, 24, 27], [47, 54, 61]])
});

/* gather tests*/

test('gather - should accumulate strings and return in order', () => {
    const result1 = gather("a")("b")("c").order(0)(1)(2).get();
    expect(result1).toBe("abc");

    const result2 = gather("a")("b")("c").order(2)(1)(0).get();
    expect(result2).toBe("cba");

    const result3 = gather("e")("l")("o")("l")("!")("h").order(5)(0)(1)(3)(2)(4).get();
    expect(result3).toBe("hello!");
});

test('gather - should handle empty arguments', () => {
    const result = gather().order(0)(1)(2).get();
    expect(result).toBe("");
});


test('gather - should handle empty "order" call', () => {
    const result = gather("a")("b")("c").order().get();
    expect(result).toBe("abc");
});